
package jp.alhinc.miyazaki_konosuke.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class FileReaderBranchList {
	public static void main(String[] args) {

		// コマンドライン引数チェック
		if (args.length != 1) {
			System.out.println("予期せぬエラーが発生しました"); // ErrNo1,2
			return;
		}

		// （3-1-ⅰ）支店定義ファイルが存在するかどうかのエラーチェック
		File file2 = new File(args[0], "branch.lst");
		if (!file2.exists()) {
			System.out.println("支店定義ファイルが存在しません"); // ErrNo3
			return;
		}

		HashMap<String, String> map = new HashMap<String, String>(); // ①支店コード（キー）と支店名（バリュー）
		HashMap<String, Long> sellsmap = new HashMap<String, Long>(); // ②支店コードと売り上げ

		String path = args[0];
		// 支店リスト読み込みメソッドの呼び出し
		if(listReader(path,"branch.lst", map, sellsmap) == false){
			return;
		}
		// 支店リスト読み込みメソッドからの戻り値を渡す
		// ListReader(args[0],storenum);

		// 売上ファイル連番チェック
		File dir = new File(path);
		File[] files = dir.listFiles();

		ArrayList<File> rcdFiles = new ArrayList<File>();

		for (int i = 0; i < files.length; i++) {
			if (files[i].getName().matches("^[0-9]{8}.rcd$") && files[i].isFile()) {
				rcdFiles.add(files[i]);
			}
		}

		// rcdFilesのなかはrcdFileだけ
		for (int i = 0; i < rcdFiles.size() - 1; i++) {
			int x = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int y = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));

			// 売り上げファイル連番チェック ErrNo14
			if ((y - x) != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}

		}

		// 売上ファイルの集計
		for (int i = 0; i < rcdFiles.size(); i++) {
			ArrayList<String> sales = new ArrayList<String>();

			BufferedReader br1 = null;

			try {
				File file = rcdFiles.get(i);
				FileReader fr1 = new FileReader(file);
				br1 = new BufferedReader(fr1);
				String line;

				// 売り上げファイルのラインの読み込み
				while ((line = br1.readLine()) != null) {
					sales.add(line);
				}

				// （3-2-ⅳ）売り上げファイルが2行以外の場合、エラー処理
				if (sales.size() != 2) {
					System.out.println(file.getName() + "のフォーマットが不正です"); // ErrNo25,26
					return;
				}

				// 売上ファイルの支店コードが支店定義ファイルに存在しない場合
				if (map.get(sales.get(0)) == null) {
					System.out.println(file.getName() + "の支店コードが不正です"); // ErrNo24
					return;
				}

				// 売上金額の数値チェック
				if (!sales.get(1).matches("^[0-9]*$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				// System.out.println(file);
				// System.out.println("ファイル名:" + file.getName() + "支店:" +
				// sales.get(0) + "/売上:" + sales.get(1));


				long sum = Long.parseLong(sales.get(1)) + sellsmap.get(sales.get(0));
				// （3-2-ⅱ）売り上げ合計金額が10桁オーバーの場合のエラー処理
				if (11 <= String.valueOf(sum).length()) {
					System.out.println("合計金額が10桁を超えました"); // ErrNo23
					return;
				}

				sellsmap.put(sales.get(0), sum);

			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if (br1 != null) {
					try {
						br1.close();
					} catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}

		}

		// 売り上げ集計結果をファイルに出力するメソッド呼び出し
		if(writerOut(path, "branch.out", map, sellsmap) == false){
			return;
		}
//		 System.out.println("【売上集計結果】\r\n" + sellsmap);

	}

	// 支店リスト読み込みメソッド
	public static boolean listReader(String dirPath,String writerFile, HashMap<String, String> branchmap,
			HashMap<String, Long> salesMap) {

		// System.out.println("ここにあるファイルを開きます=>" + args[0]);
		BufferedReader br = null;
		try {
			File file = new File(dirPath, writerFile);
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			String[] storenum = null;
			// 1行ずつファイルを読み込む
			while ((line = br.readLine()) != null) {
				storenum = line.split(",", 0);

				// ②（3-1-ⅱ）支店定義ファイルのフォーマットのエラー処理
				if (!storenum[0].matches("^[0-9]{3}$") || (storenum.length !=2)) {
					System.out.println("支店定義ファイルのフォーマットが不正です"); // ErrNo5-11,12,13
					return false;
				}
				branchmap.put(storenum[0], storenum[1]);
				// 支店、売上初期値zero
				salesMap.put(storenum[0], (long) 0);
				// System.out.println(sellsmap);

				// for (String elem : storenum) {
				// // System.out.println(elem);

			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	// 売り上げ集計結果をファイルに出力するメソッド
	public static boolean writerOut(String dirPath, String writerFile, HashMap<String, String> branchmap,
			HashMap<String, Long> salesMap) {

		// 各店舗の集計を出力する
		BufferedWriter bw = null;
		try {
			File fileWf = new File(dirPath, writerFile);
			bw = new BufferedWriter(new FileWriter(fileWf));

			// Hashmapをゲットし、sellsmapから出力する

			// 拡張for分によってすべて実行する
			for (String q : salesMap.keySet()) {
				bw.write(q + "," + branchmap.get(q) + "," + salesMap.get(q) + "\r\n");
				// System.out.println(q + "," + map.get(q) + "," +
				// sellsmap.get(q));
			}

		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}

		}
		return true;
	}

}
